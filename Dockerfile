FROM debian:jessie

# Update
RUN apt-get update -y \
# Install pip
    && apt-get install -y \
        swig \
        python-pip \
# Install deps for backports.lzma (python2 requires it)
        python-dev \
        python-mysqldb \
        python-rsa \
        libssl-dev \
        liblzma-dev \
	libevent-dev \
    && rm -rf /var/lib/apt/lists/*

#        libevent1-dev \
COPY . /docker-registry
COPY ./config/boto.cfg /etc/boto.cfg
COPY ./pip-20.3.4-py2.py3-none-any.whl .

# Install core
RUN pip install pip-20.3.4-py2.py3-none-any.whl
RUN pip install --index-url https://pypi.org/simple /docker-registry/depends/docker-registry-core

# Install registry
RUN pip install file:///docker-registry#egg=docker-registry[bugsnag,newrelic,cors]

# patch the SSLv3 problem of gevent
RUN f=`python -c "import ssl; import os; print os.path.dirname(ssl.__file__)"` && echo "PROTOCOL_SSLv3 = PROTOCOL_SSLv23" >> $f/ssl.py

ENV DOCKER_REGISTRY_CONFIG /docker-registry/config/config_sample.yml
ENV SETTINGS_FLAVOR dev

EXPOSE 5000

CMD ["docker-registry"]
